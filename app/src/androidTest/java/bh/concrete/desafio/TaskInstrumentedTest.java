package bh.concrete.desafio;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import bh.concrete.desafio.infra.application.RealmInstancia;
import bh.concrete.desafio.infra.task.TaskGitHubPull;
import bh.concrete.desafio.infra.task.TaskGitHubRepository;
import bh.concrete.desafio.infra.task.params.ParamsPull;
import bh.concrete.desafio.infra.task.params.ParamsRepository;
import bh.concrete.desafio.model.persist.Pull;
import bh.concrete.desafio.model.persist.Repository;
import bh.concrete.desafio.view.activity.MainActivity;
import bh.concrete.desafio.view.activity.PullActivity;

import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.junit.Assert.*;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class TaskInstrumentedTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule =
            new ActivityTestRule<>(MainActivity.class);

    @Rule
    public ActivityTestRule<PullActivity> mPullRule =
            new ActivityTestRule<>(PullActivity.class);

    @Test
    public void repository() throws Exception {
        new TaskGitHubRepository(mActivityRule.getActivity()).execute(new ParamsRepository("language:Java", "stars", 1));
        assertTrue(!RealmInstancia.getInstancia().where(Repository.class).findAll().isEmpty());
    }

    @Test
    public void pull() throws Exception {
        ParamsPull paramsPull = new ParamsPull("elastic", "elasticsearch", 1);
        new TaskGitHubPull(mPullRule.getActivity()).execute(paramsPull);
        assertTrue(!RealmInstancia.getInstancia().where(Pull.class).equalTo("repo", "elasticsearch").findAll().isEmpty());
    }
}
