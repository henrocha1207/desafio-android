package bh.concrete.desafio.infra.task;

import android.support.test.rule.ActivityTestRule;

import org.junit.Rule;
import org.junit.Test;

import bh.concrete.desafio.infra.application.Application;
import bh.concrete.desafio.infra.task.params.ParamsRepository;
import bh.concrete.desafio.infra.util.Constants;
import bh.concrete.desafio.view.activity.MainActivity;

import static org.junit.Assert.*;

/**
 * Created by henrique on 09/03/17.
 */
public class TaskGitHubPullTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void doInBackground() throws Exception {
        MainActivity activity = mActivityRule.getActivity();
        new TaskGitHubRepository(activity).execute(new ParamsRepository("language:Java", "stars", Application.getPreferenceManager().getIntegerValuePreferences(Constants.Preferences.PAGE)));
    }

}