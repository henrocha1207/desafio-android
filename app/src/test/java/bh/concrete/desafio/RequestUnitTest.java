package bh.concrete.desafio;

import android.content.Context;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;

import java.io.IOException;
import java.util.List;

import bh.concrete.desafio.infra.application.Application;
import bh.concrete.desafio.infra.request.client.GitHubService;
import bh.concrete.desafio.infra.task.TaskGitHubRepository;
import bh.concrete.desafio.infra.task.params.ParamsPull;
import bh.concrete.desafio.infra.task.params.ParamsRepository;
import bh.concrete.desafio.infra.util.Constants;
import bh.concrete.desafio.infra.util.ToastyUtils;
import bh.concrete.desafio.model.persist.Pull;
import bh.concrete.desafio.model.persist.Repository;
import bh.concrete.desafio.view.activity.MainActivity;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static org.junit.Assert.*;
import static org.mockito.Mockito.spy;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class RequestUnitTest {

    @Test
    public void requestRepositories() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.github.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        GitHubService service = retrofit.create(GitHubService.class);

        List<Repository> repositoryListgitResult = null;
       ParamsRepository params = new ParamsRepository("language:Java", "stars", 1);
        try {
            repositoryListgitResult = service.request(params.getQ(), params.getSort(), params.getPage()).execute().body().getItems();

            assertFalse(repositoryListgitResult.isEmpty());
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void requestPullRepositories() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.github.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        GitHubService service = retrofit.create(GitHubService.class);

        List<Pull> pullListgitResult = null;
        ParamsPull paramsPull = new ParamsPull("elastic", "elasticsearch", 1);
        try {
            pullListgitResult = service.pullRequest(paramsPull.getOwner(), paramsPull.getRepository(), paramsPull.getPage().toString()).execute().body();
            assertTrue(!pullListgitResult.isEmpty());
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

}