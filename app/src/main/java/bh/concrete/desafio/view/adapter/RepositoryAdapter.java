package bh.concrete.desafio.view.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import bh.concrete.desafio.view.activity.PullActivity;
import bh.concrete.desafio.R;
import bh.concrete.desafio.infra.application.Application;
import bh.concrete.desafio.infra.task.TaskGitHubRepository;
import bh.concrete.desafio.infra.task.TaskGitHubUser;
import bh.concrete.desafio.infra.task.params.ParamsRepository;
import bh.concrete.desafio.infra.util.Constants;
import bh.concrete.desafio.model.persist.Repository;
import bh.concrete.desafio.model.persist.User;
import io.realm.Realm;
import io.realm.RealmRecyclerViewAdapter;
import io.realm.RealmResults;

/**
 * Created by henrique on 09/03/17.
 *
 * Adapter responsável pelo recyclerview main
 */
public class RepositoryAdapter extends RealmRecyclerViewAdapter<Repository, RepositoryAdapter.ViewHolder> {

    private ImageLoader imageLoader;
    private Activity activity;

    public RepositoryAdapter(RealmResults<Repository>  data, boolean autoUpdate, Activity activity) {
        super(data, autoUpdate);
        initImageLoader();
        this.activity = activity;
    }


    /**
     * Inicializar imageloader com cache em memoria
     *
     */
    public void initImageLoader(){
        if(imageLoader !=null){
            imageLoader.destroy();
        }

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(Application.getContext())
                .threadPoolSize(5)
                .threadPriority(Thread.MIN_PRIORITY + 2)
                .defaultDisplayImageOptions(DisplayImageOptions.createSimple())
                .build();

        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .build();

        imageLoader = ImageLoader.getInstance();
        imageLoader.init(config);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_repository, parent, false);
        return new ViewHolder(itemView);
    }

    /**
     * Atribui o usário, caso ele já esteja no realm ele elimina a necessidade de requisição
     * @param holder
     * @param login
     */
    public void searchUser(ViewHolder holder, String login){
        User user = Realm.getDefaultInstance().where(User.class).equalTo("login", login).findFirst();
        if(user != null){
            holder.nomeAutorTv.setText(user.getName());
        }else{
            new TaskGitHubUser().execute(login);
        }
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Repository obj = getData().get(position);
        holder.data = obj;
        holder.nameTv.setText(obj.getName());
        holder.descriptionTv.setText(obj.getDescription());
        holder.forkTv.setText(obj.getFork().toString());
        holder.starsTv.setText(obj.getStars().toString());
        holder.autorTv.setText(obj.getOwner().getLogin());
        imageLoader.displayImage(obj.getOwner().getAvatar(), holder.avatarIV);

        searchUser(holder, obj.getOwner().getLogin());
        if(position == getItemCount()-1){
            Integer page = Application.getPreferenceManager().getIntegerValuePreferences(Constants.Preferences.PAGE);
            page = page+1;
            Application.getPreferenceManager().savePreference(Constants.Preferences.PAGE, page);
            new TaskGitHubRepository(activity).execute(new ParamsRepository("language:Java", "stars", page));
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView nameTv;
        public TextView descriptionTv;
        public TextView forkTv;
        public TextView starsTv;
        public ImageView avatarIV;
        public TextView autorTv;
        public TextView nomeAutorTv;
        public Repository data;

        public ViewHolder(View view) {
            super(view);

            view.setOnClickListener(this);
            nameTv = (TextView) view.findViewById(R.id.name);
            descriptionTv = (TextView) view.findViewById(R.id.description);
            forkTv = (TextView) view.findViewById(R.id.forks);
            starsTv = (TextView) view.findViewById(R.id.stars);
            avatarIV = (ImageView) view.findViewById(R.id.avatar);
            autorTv = (TextView) view.findViewById(R.id.autorTv);
            nomeAutorTv = (TextView) view.findViewById(R.id.nameAutorTv);
        }


        /**
         * Abrir activity com os PUlls
         * @param v
         */
        @Override
        public void onClick(View v) {
            Intent i = new Intent(Application.getContext(), PullActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            i.putExtra(Constants.ParamIntent.OWNER, data.getOwner().getLogin());
            i.putExtra(Constants.ParamIntent.REPOSITORY, data.getName());
            Application.getContext().startActivity(i);
        }
    }
}
