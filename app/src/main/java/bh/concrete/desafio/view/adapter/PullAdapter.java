package bh.concrete.desafio.view.adapter;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import bh.concrete.desafio.R;
import bh.concrete.desafio.infra.application.Application;
import bh.concrete.desafio.infra.task.TaskGitHubPull;
import bh.concrete.desafio.infra.task.TaskGitHubUser;
import bh.concrete.desafio.infra.task.params.ParamsPull;
import bh.concrete.desafio.infra.util.Constants;
import bh.concrete.desafio.model.persist.Pull;
import bh.concrete.desafio.model.persist.User;
import io.realm.Realm;
import io.realm.RealmRecyclerViewAdapter;
import io.realm.RealmResults;

/**
 * Created by henrique on 09/03/17.
 *
 * Adapter responsável pelo recyclerview main
 */
public class PullAdapter extends RealmRecyclerViewAdapter<Pull, PullAdapter.ViewHolder> {

    private ImageLoader imageLoader;

    public PullAdapter(RealmResults<Pull> data, boolean autoUpdate) {
        super(data, autoUpdate);
        initImageLoader();
    }

    /**
     * Inicializar imageloader com cache em memoria
     */
    public void initImageLoader(){
        if(imageLoader !=null){
            imageLoader.destroy();
        }

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(Application.getContext())
                .threadPoolSize(5)
                .threadPriority(Thread.MIN_PRIORITY + 2)
                .defaultDisplayImageOptions(DisplayImageOptions.createSimple())
                .build();

        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .build();

        imageLoader = ImageLoader.getInstance();
        imageLoader.init(config);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_pull, parent, false);
        return new ViewHolder(itemView);
    }



    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Pull obj = getData().get(position);

        holder.data = obj;
        holder.titleTv.setText(obj.getTitle());
        holder.descriptionTv.setText(obj.getDescription());
        holder.usernameTv.setText(obj.getUser().getLogin());

        User user = Realm.getDefaultInstance().where(User.class).equalTo("login", obj.getUser().getLogin()).findFirst();
        if(user != null){
            holder.nameTv.setText(user.getName());
        }else{
            new TaskGitHubUser().execute(obj.getUser().getLogin());
        }

        imageLoader.displayImage(obj.getUser().getAvatar(), holder.avatarIV);

        if(position == getItemCount()-1){
            Integer page = Application.getPreferenceManager().getIntegerValuePreferences(Constants.Preferences.PAGE_PULL);
            page = page+1;
            Application.getPreferenceManager().savePreference(Constants.Preferences.PAGE_PULL, page);
            new TaskGitHubPull().execute(new ParamsPull(obj.getUser().getLogin(), obj.getTitle(), Application.getPreferenceManager().getIntegerValuePreferences(Constants.Preferences.PAGE_PULL)));
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView titleTv;
        public TextView descriptionTv;
        public ImageView avatarIV;
        public TextView usernameTv;
        public TextView nameTv;

        public Pull data;

        public ViewHolder(View view) {
            super(view);

            view.setOnClickListener(this);
            titleTv = (TextView) view.findViewById(R.id.titlePull);
            descriptionTv = (TextView) view.findViewById(R.id.description);
            avatarIV = (ImageView) view.findViewById(R.id.avatar);
            usernameTv = (TextView) view.findViewById(R.id.username);
            nameTv = (TextView) view.findViewById(R.id.nameAutor);
        }


        @Override
        public void onClick(View v) {

            String url = data.getHtml_url();
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            i.setData(Uri.parse(url));
            Application.getContext().startActivity(i);
            Log.i("asdf","asdf");
        }
    }
}
