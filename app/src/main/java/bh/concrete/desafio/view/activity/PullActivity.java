package bh.concrete.desafio.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import java.util.List;

import bh.concrete.desafio.R;
import bh.concrete.desafio.infra.application.Application;
import bh.concrete.desafio.infra.application.RealmInstancia;
import bh.concrete.desafio.infra.task.TaskGitHubPull;
import bh.concrete.desafio.infra.task.params.ParamsPull;
import bh.concrete.desafio.infra.util.Constants;
import bh.concrete.desafio.model.persist.Pull;
import bh.concrete.desafio.model.persist.Repository;
import bh.concrete.desafio.view.adapter.PullAdapter;
import bh.concrete.desafio.view.adapter.RepositoryAdapter;
import io.realm.Realm;
import io.realm.RealmResults;

public class PullActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private PullAdapter pullAdapter;
    private RealmResults<Pull> pulls;
    private Realm realm;

    private String owner;
    private String repository;

    private TextView opened;
    private TextView closed;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repository);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();
        owner = intent.getStringExtra(Constants.ParamIntent.OWNER);
        repository = intent.getStringExtra(Constants.ParamIntent.REPOSITORY);

        getSupportActionBar().setTitle(repository);

        ParamsPull paramsPull = new ParamsPull(owner, repository, Application.getPreferenceManager().getIntegerValuePreferences(Constants.Preferences.PAGE_PULL));
        realm = RealmInstancia.getInstancia();
        realm.setAutoRefresh(true);

        opened = (TextView) findViewById(R.id.opened);
        closed = (TextView) findViewById(R.id.closed);

        recyclerView = (RecyclerView) findViewById(R.id.rvPull);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        pulls = RealmInstancia.getInstancia().where(Pull.class).equalTo("repo", repository).findAll();
        pullAdapter = new PullAdapter(pulls, true);
        recyclerView.setAdapter(pullAdapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        Application.getPreferenceManager().savePreference(Constants.Preferences.PAGE_PULL, 1);

        new TaskGitHubPull(this).execute(paramsPull);
    }


    /**
     * Remonta recycler view e atualiza os valores dos indices
     *
     * @param pullList
     */
    public void refreshAdapter(final List<Pull> pullList) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                RealmInstancia.getInstancia().executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        realm.copyToRealmOrUpdate(pullList);
                    }
                });

                pulls = RealmInstancia.getInstancia().where(Pull.class).equalTo("repo", repository).findAll();
                pullAdapter.notifyDataSetChanged();

                long contOpened = RealmInstancia.getInstancia().where(Pull.class).equalTo("repo", repository).equalTo("state","open").count();
                long contClosed = RealmInstancia.getInstancia().where(Pull.class).equalTo("repo", repository).equalTo("state","close").count();

                opened.setText(contOpened + " opened ");
                closed.setText("/ " + contClosed + " closed ");
            }
        });
    }

}
