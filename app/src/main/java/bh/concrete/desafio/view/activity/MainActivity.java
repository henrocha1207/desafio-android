package bh.concrete.desafio.view.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.util.List;

import bh.concrete.desafio.R;
import bh.concrete.desafio.infra.application.Application;
import bh.concrete.desafio.infra.application.RealmInstancia;
import bh.concrete.desafio.infra.task.TaskGitHubRepository;
import bh.concrete.desafio.infra.task.params.ParamsRepository;
import bh.concrete.desafio.infra.util.Constants;
import bh.concrete.desafio.model.persist.Repository;
import bh.concrete.desafio.model.value.GitResult;
import bh.concrete.desafio.view.adapter.RepositoryAdapter;
import io.realm.Realm;
import io.realm.RealmResults;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private RecyclerView recyclerView;
    private RepositoryAdapter repositoryAdapter;
    private RealmResults<Repository> repositories;
    private Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.title_main);

        realm = RealmInstancia.getInstancia();
        realm.setAutoRefresh(true);
        init();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }


    /**
     * Método responsável por iniciar os componentes da tela
     *
     */
    private void init() {

        recyclerView = (RecyclerView) findViewById(R.id.recycler_repository);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        repositories = realm.where(Repository.class).findAll();
        repositoryAdapter = new RepositoryAdapter(repositories, true, this);
        recyclerView.setAdapter(repositoryAdapter);


        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        new TaskGitHubRepository(this).execute(new ParamsRepository("language:Java", "stars", Application.getPreferenceManager().getIntegerValuePreferences(Constants.Preferences.PAGE)));
    }

    /**
     * Mostra ou esconde mensagem de nenhum reposotorio
     */
    public void toggleEmptyMessage(){
        if(repositories.isEmpty()){
            recyclerView.setVisibility(View.GONE);
            findViewById(R.id.btnempty).setVisibility(View.VISIBLE);

            final Activity activity = this;
            findViewById(R.id.btnempty).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new TaskGitHubRepository(activity).execute(new ParamsRepository("language:Java", "stars", Application.getPreferenceManager().getIntegerValuePreferences(Constants.Preferences.PAGE)));
                }
            });
        }else{
            recyclerView.setVisibility(View.VISIBLE);
            findViewById(R.id.btnempty).setVisibility(View.GONE);
        }

    }

    /**
     *  Delegate responsável por persistir os dados e atualizar a tela
     *
     * @param listRepo
     */
    public void refreshAdapter(final List<Repository> listRepo){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                RealmInstancia.getInstancia().executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        RealmInstancia.getInstancia().copyToRealmOrUpdate(listRepo);
                    }
                });
                repositories = RealmInstancia.getInstancia().where(Repository.class).findAll();
                repositoryAdapter.notifyDataSetChanged();
                toggleEmptyMessage();
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.nav_inicio) {
           // new TaskGitHubRepository().execute(new ParamsRepository("language:Java", "stars", Application.getPreferenceManager().getIntegerValuePreferences(Constants.Preferences.PAGE)));
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public RealmResults<Repository> getRepositories() {
        return repositories;
    }

    public void setRepositories(RealmResults<Repository> repositories) {
        this.repositories = repositories;
    }
}
