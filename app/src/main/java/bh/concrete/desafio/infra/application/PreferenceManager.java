package bh.concrete.desafio.infra.application;

import android.content.SharedPreferences;

/**
 *  Created by hrocha on 08/03/17.
 *
 * Utilitario para salvar preferences
 */

public class PreferenceManager {

    private SharedPreferences sharedPref;

    public PreferenceManager(SharedPreferences sharedPref) {
        this.sharedPref = sharedPref;
    }

    /**
     *  Salvar inteiro
     * @param key
     * @param value
     */
    public void savePreference(String key, Integer value){
        SharedPreferences.Editor editor = getSharedPref().edit();
        editor.putInt(key, value);
        editor.apply();
    }


    /**
     * Return string value of key
     * @param key
     * @return
     */
    public Integer getIntegerValuePreferences(String key) {
        return sharedPref.getInt(key, 1);
    }


    /**
     * Remove all preferences
     */
    public void clear(){
        SharedPreferences.Editor editor = getSharedPref().edit();
        editor.clear().apply();
    }

    public SharedPreferences getSharedPref() {
        return sharedPref;
    }

    public void setSharedPref(SharedPreferences sharedPref) {
        this.sharedPref = sharedPref;
    }
}
