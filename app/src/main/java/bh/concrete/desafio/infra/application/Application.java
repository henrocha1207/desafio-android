package bh.concrete.desafio.infra.application;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;

import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import bh.concrete.desafio.infra.util.Constants;
import io.realm.Realm;

/**
 * Created by hrocha on 08/03/17.
 *
 * Classe de aplicação
 *
 *
 */
public class Application extends android.app.Application {

    private static Context context;
    private static PreferenceManager preferenceManager;

    /**
     * Inicia contexto de preferences e realm
     */
    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
        Realm.init(context);
        preferenceManager = new PreferenceManager(getSharedPreferences(Constants.Preferences.SHARED_PREFERENCES, Context.MODE_PRIVATE));
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    public static Context getContext() {
        return context;
    }

    public static void setContext(Context context) {
        Application.context = context;
    }

    public static PreferenceManager getPreferenceManager() {
        return preferenceManager;
    }

    public static void setPreferenceManager(PreferenceManager preferenceManager) {
        Application.preferenceManager = preferenceManager;
    }
}
