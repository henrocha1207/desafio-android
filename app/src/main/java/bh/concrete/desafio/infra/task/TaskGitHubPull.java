package bh.concrete.desafio.infra.task;

import android.app.Activity;
import android.os.AsyncTask;

import java.util.List;

import bh.concrete.desafio.view.activity.PullActivity;
import bh.concrete.desafio.R;
import bh.concrete.desafio.infra.application.Application;
import bh.concrete.desafio.infra.request.client.GitHubService;
import bh.concrete.desafio.infra.task.params.ParamsPull;
import bh.concrete.desafio.infra.util.Constants;
import bh.concrete.desafio.infra.util.ToastyUtils;
import bh.concrete.desafio.model.persist.Pull;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by henrique on 09/03/17.
 *
 * Asynctaskk de atualiação dos pulls por usuário
 *
 */
public class TaskGitHubPull extends AsyncTask<ParamsPull, String, Boolean> {

    private Activity activity;

    public TaskGitHubPull(Activity activity) {
        this.activity = activity;
    }

    public TaskGitHubPull() {
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        ToastyUtils.showToasty(R.string.update_message);
    }

    @Override
    protected Boolean doInBackground(final ParamsPull... params) {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.github.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        GitHubService service = retrofit.create(GitHubService.class);
        List<Pull> pullList = null;
        try {
            pullList = service.pullRequest(params[0].getOwner(), params[0].getRepository(), params[0].getPage().toString()).execute().body();

            if(!pullList.isEmpty()){
                for (Pull pl : pullList) {
                    pl.setRepo(params[0].getRepository());
                }
            }

            ((PullActivity) activity).refreshAdapter(pullList);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return pullList != null;
    }

    @Override
    protected void onPostExecute(Boolean sucesso) {
        super.onPostExecute(sucesso);
        if (!sucesso) {
            Integer page = Application.getPreferenceManager().getIntegerValuePreferences(Constants.Preferences.PAGE_PULL);
            Application.getPreferenceManager().savePreference(Constants.Preferences.PAGE_PULL, page-1);
            ToastyUtils.showToasty(R.string.erro_message);
        }
    }


}
