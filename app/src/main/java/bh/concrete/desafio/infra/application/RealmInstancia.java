package bh.concrete.desafio.infra.application;

import bh.concrete.desafio.infra.util.Constants;
import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 *  Created by hrocha on 09/03/17.
 *
 *
 *  Sigleton para buscar a mesma instancia do realm evitando multiplos contextos
 */
public class RealmInstancia {

    private static RealmConfiguration realmConfiguration;

    private RealmInstancia() {
    }

    public static Realm getInstancia(){
        if(realmConfiguration == null ){
            realmConfiguration = new RealmConfiguration.Builder().name(Constants.Preferences.DB_NAME).build();
        }
        return Realm.getInstance(realmConfiguration);
    }

}
