package bh.concrete.desafio.infra.task.params;

import java.io.Serializable;

/**
 * Created by henrique on 09/03/17.
 *
 * Paramentros repassados ao task de busca de repositorios
 */
public class ParamsRepository implements Serializable {

    private String q;
    private String sort;
    private Integer page;

    public ParamsRepository(String q, String sort, Integer page) {
        this.q = q;
        this.sort = sort;
        this.page = page;
    }

    public String getQ() {
        return q;
    }

    public void setQ(String q) {
        this.q = q;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }
}
