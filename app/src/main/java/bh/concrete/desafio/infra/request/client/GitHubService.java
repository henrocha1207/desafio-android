package bh.concrete.desafio.infra.request.client;

import java.util.List;

import bh.concrete.desafio.model.value.GitResult;
import bh.concrete.desafio.model.persist.Pull;
import bh.concrete.desafio.model.persist.User;
import bh.concrete.desafio.model.value.PullResult;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 *  Created by hrocha on 09/03/17.
 *
 *  Classe do endereçamento dos serviços
 */
public interface GitHubService {

    @Headers("Authorization: token 6ad7d350b8fc6aefb47b1aa594399e1fad0be9be")
    @GET("/search/repositories")
    Call<GitResult> request(
            @Query("q") String q,
            @Query("sort") String sort,
            @Query("page") int page
    );

    @Headers("Authorization: token 6ad7d350b8fc6aefb47b1aa594399e1fad0be9be")
    @GET("/u/{user}?v=3")
    Call<GitResult> avatar(@Path("user") String user, @Query("v") int v);

    @Headers("Authorization: token 6ad7d350b8fc6aefb47b1aa594399e1fad0be9be")
    @GET("/repos/{owner}/{repository}/pulls")
    Call<List<Pull>> pullRequest(@Path("owner") String owner, @Path("repository") String repository, @Query("page") String page);

    @Headers("Authorization: token 6ad7d350b8fc6aefb47b1aa594399e1fad0be9be")
    @GET("/users/{user}")
    Call<User> userRequest(@Path("user") String user);


}
