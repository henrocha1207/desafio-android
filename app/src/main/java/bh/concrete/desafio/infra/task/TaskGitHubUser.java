package bh.concrete.desafio.infra.task;

import android.os.AsyncTask;

import java.io.IOException;

import bh.concrete.desafio.R;
import bh.concrete.desafio.infra.request.client.GitHubService;
import bh.concrete.desafio.infra.util.ToastyUtils;
import bh.concrete.desafio.model.persist.User;
import io.realm.Realm;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by henrique on 09/03/17.
 *
 *  Asynctaskk de atualiação dos usuarios
 */
public class TaskGitHubUser extends AsyncTask<String, String, Void> {


    @Override
    protected Void doInBackground(String... params) {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.github.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        GitHubService service = retrofit.create(GitHubService.class);

        User user = null;
        try {
            user = service.userRequest(params[0]).execute().body();
            if(user != null) {
                final User finalUser = user;
                Realm.getDefaultInstance().executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        realm.copyToRealmOrUpdate(finalUser);
                    }
                });
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
