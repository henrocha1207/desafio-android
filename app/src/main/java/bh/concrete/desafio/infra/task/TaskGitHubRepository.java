package bh.concrete.desafio.infra.task;

import android.app.Activity;
import android.os.AsyncTask;

import java.io.IOException;
import java.util.List;

import bh.concrete.desafio.infra.application.Application;
import bh.concrete.desafio.infra.task.params.ParamsRepository;
import bh.concrete.desafio.infra.util.Constants;
import bh.concrete.desafio.model.persist.Repository;
import bh.concrete.desafio.view.activity.MainActivity;
import bh.concrete.desafio.R;
import bh.concrete.desafio.infra.request.client.GitHubService;
import bh.concrete.desafio.infra.util.ToastyUtils;
import bh.concrete.desafio.model.value.GitResult;
import io.realm.Realm;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by henrique on 09/03/17.
 * <p>
 * Asynctaskk de atualiação dos repositorios por usuário
 */
public class TaskGitHubRepository extends AsyncTask<ParamsRepository, String, Boolean> {

    private Activity activity;

    public TaskGitHubRepository(Activity activity) {
        this.activity = activity;
    }

    /**
     * Mostra mensagem de 'busca'
     */
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        ToastyUtils.showToasty(R.string.update_message);
    }

    @Override
    protected Boolean doInBackground(ParamsRepository... params) {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.github.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        GitHubService service = retrofit.create(GitHubService.class);

        List<Repository> repositoryListgitResult = null;
        try {
            repositoryListgitResult = service.request(params[0].getQ(), params[0].getSort(), params[0].getPage()).execute().body().getItems();
        } catch (IOException e) {
            ToastyUtils.showToasty(R.string.erro_message);
        }
        ((MainActivity) activity).refreshAdapter(repositoryListgitResult);
        return repositoryListgitResult != null;
    }

    /**
     * caso não tenha sucesso método dispara uma mensagem de erro e decrementa a pagina
     * @param sucesso
     */
    @Override
    protected void onPostExecute(Boolean sucesso) {
        super.onPostExecute(sucesso);
        if (!sucesso) {
            Integer page = Application.getPreferenceManager().getIntegerValuePreferences(Constants.Preferences.PAGE);
            Application.getPreferenceManager().savePreference(Constants.Preferences.PAGE, page-1);
            ToastyUtils.showToasty(R.string.erro_message);
        }
        try {
            this.finalize();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }
}
