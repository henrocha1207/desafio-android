package bh.concrete.desafio.infra.util;

/**
 * Created by henrique on 09/11/16.
 *
 * Arquivo de constantes
 */
public final class Constants {

    public static final class Preferences{
        public static final String SHARED_PREFERENCES = "QAppSharedPref";
        public static final String PAGE = "page";
        public static final String PAGE_PULL = "page_pull";

        public static final String DB_NAME = "concrete";
    }

    public static final class ParamIntent{
        public static final String OWNER = "owner";
        public static final String REPOSITORY = "repository";
    }


}
