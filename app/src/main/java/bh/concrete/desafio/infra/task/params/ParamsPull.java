package bh.concrete.desafio.infra.task.params;

import java.io.Serializable;

import retrofit2.http.GET;

/**
 * Created by henrique on 09/03/17.
 *
 * Parametros pull asynctask
 */
public class ParamsPull {

    private String owner;
    private String repository;
    private Integer page;

    public ParamsPull(String owner, String repository, Integer page) {
        this.owner = owner;
        this.repository = repository;
        this.page = page;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getRepository() {
        return repository;
    }

    public void setRepository(String repository) {
        this.repository = repository;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }
}
