package bh.concrete.desafio.infra.util;

import android.os.Handler;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.Toast;

import bh.concrete.desafio.infra.application.Application;


/**
 * Created by henrique on 08/11/16.
 *
 * Classe utilitária de exibição de toasty
 */
public class ToastyUtils {


    /**
     * Metodo responsável por exibir uma mensagem via tosaty na tela
     *
     * @param intMessage
     */
    public static void showToasty(final int intMessage) {

        Handler mainHandler = new Handler(Application.getContext().getMainLooper());
        Runnable myRunnable = new Runnable() {
            @Override
            public void run() {
                Toast toast = Toast.makeText(Application.getContext(), Application.getContext().getResources().getString(intMessage), Toast.LENGTH_LONG);
                toast.show();
            }
        };
        mainHandler.post(myRunnable);
    }

}
