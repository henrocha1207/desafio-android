package bh.concrete.desafio.model.value;

import java.util.List;

import bh.concrete.desafio.model.persist.Pull;
import bh.concrete.desafio.model.persist.Repository;

/**
 * Created by henrique on 08/03/17.
 */

public class PullResult {

    public List<Pull> items;

    public List<Pull> getItems() {
        return items;
    }

    public void setItems(List<Pull> items) {
        this.items = items;
    }
}
