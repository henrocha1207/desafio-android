package bh.concrete.desafio.model.value;

import java.util.List;

import bh.concrete.desafio.model.persist.Repository;

/**
 * Created by hrocha on 06/03/17.
 */

public class GitResult {

    public List<Repository> items;

    public List<Repository> getItems() {
        return items;
    }

    public void setItems(List<Repository> items) {
        this.items = items;
    }
}
